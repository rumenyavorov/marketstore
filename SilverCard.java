package MarketStore;

public class SilverCard extends Card {

  public SilverCard(double turnover, double purchaseValue, Owner owner) {
    super(turnover, purchaseValue, owner);
    this.setDiscountRate();
  }

  public void setDiscountRate() {
    if(super.getTurnover() > 300) {
      super.setDiscountRate(0.035);
    } else {
      super.setDiscountRate(0);
    }
  }
}