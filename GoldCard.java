package MarketStore;

public class GoldCard extends Card {

  public GoldCard(double turnover, double purchaseValue, Owner owner) {
    super(turnover, purchaseValue, owner);
    setDiscountRate();
  }

  public void setDiscountRate() {
   if(super.getTurnover() / 100 + 2 > 10) {
    setDiscountRate(0.10);
   } else {
     setDiscountRate((super.getTurnover() / 100 + 2) / 100);
   }
  }
}