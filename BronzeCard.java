package MarketStore;

public class BronzeCard extends Card {

  public BronzeCard(double turnover, double purchaseValue, Owner owner) {
    super(turnover, purchaseValue, owner);
    this.setDiscountRate();
  }

  public void setDiscountRate() {
    if(super.getTurnover() > 100 && super.getTurnover() < 300) {
      super.setDiscountRate(0.1);
      System.out.println(super.getDiscountRate());
    } else if(super.getTurnover() > 300) {
      super.setDiscountRate(0.25);
    }
  }
}