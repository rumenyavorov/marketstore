package MarketStore;

public class Main {
  public static void main(String[] args) {
    Owner owner = new Owner("Rumen", 22);

    System.out.println("Bronze Card");
    Card bronzeCard = new BronzeCard(0, 150, owner);
    bronzeCard.calculate();
    bronzeCard.print(bronzeCard);

    System.out.println("Silver Card");
    Card silverCard = new SilverCard(600, 850, owner);
    silverCard.calculate();
    silverCard.print(silverCard);

    System.out.println("Gold Card");
    Card goldCard = new GoldCard(1500, 1300, owner);
    goldCard.calculate();
    goldCard.print(goldCard);
  }
}