package MarketStore;

public class Card {
  private Owner owner;
  private double turnover;
  private double purchaseValue;
  private double discountRate;

  public Card(double turnover, double purchaseValue, Owner owner) {
    setTurnover(turnover);
    setPurchaseValue(purchaseValue);
    setOwner(owner);
  }

  double calculate() {
    return getPurchaseValue() * getDiscountRate();
  }

  void print(Object object) {
    System.out.println(object.toString());
  }

  public double getDiscountRate() {
    return this.discountRate;
  }

  public void setDiscountRate(double discountRate) {
    this.discountRate = discountRate;
  }

  public Owner getOwner() {
    return this.owner;
  }

  public void setOwner(Owner owner) {
    this.owner = owner;
  }

  public double getTurnover() {
    return this.turnover;
  }

  public void setTurnover(double turnover) {
    this.turnover = turnover;
  }

  public double getPurchaseValue() {
    return this.purchaseValue;
  }

  public void setPurchaseValue(Double purchaseValue) {
    this.purchaseValue = purchaseValue;
  };

  @Override
  public String toString() {
    return String.format(" Purchase value: $%.2f \n Discount rate: %.1f%%  \n Discount: $%.2f \n Total: $%.2f \n", 
      getPurchaseValue(), getDiscountRate() * 100, calculate(), getPurchaseValue() - calculate());
  }
}